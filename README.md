# QCM fin de session

Pour valider cette session, vous allez remplir un QCM.
Vos réponses devront être poussées dans un fichier texte sur le [projet GitLab](https://gricad-gitlab.univ-grenoble-alpes.fr/bartolel/geomass_2024).
Vous ferez une branche nommée du style "nom_prenom".
- C.lle-ci ne contiendra qu'un seul fichier, celui contenant vos réponses.

- C. fichier devra avoir la forme suivante :

```markdown
# NOM prénom
X
Y
Z
```

où X, Y, Z représentent les réponses aux questions.
Une [MR](https://gricad-gitlab.univ-grenoble-alpes.fr/bartolel/geomass_2024/-/merge_requests/1) d'exemple est déjà présente pour vous aider.

- A.tention, la correction se fait via un script sensible à la casse.
Veuillez à bien noter les réponses par une majuscule.
Il n'y a qu'une bonne réponse par question.

## Questions

### 1. Qu'est-ce que signifie l'acronyme WKT dans le contexte des systèmes d'information géographique (SIG)?
- A. Well-Known Text
- B. World Knowledge Text
- C. Wide Knowledge Transfer
- D. Worldly Known Terrain

### 2. Quel est l'avantage principal de l'utilisation de WKB par rapport à WKT pour stocker des géométries dans une base de données géospatiale?
- A. WKB est plus lisible pour les humains
- B. WKB permet une représentation plus compacte des données
- C. WKB est plus facile à éditer
- D. WKB peut stocker des métadonnées supplémentaires

### 3. Qu'est-ce que signifie l'acronyme OGC dans le contexte des systèmes d'information géographique (SIG)?
- A. Open Geospatial Consortium
- B. Organisation Géographique des Continents
- C. Organisation Globale de Cartographie
- D. Open Geographical Center

### 4. Quel est l'un des objectifs principaux des règles OGC dans le domaine des SIG?
- A. Promouvoir l'utilisation exclusive de logiciels propriétaires pour la gestion des données géographiques
- B. Contrôler strictement l'accès aux données géographiques sensibles
- C. Standardiser les formats de données géographiques pour assurer l'interopérabilité entre les systèmes
- D. Faciliter la création de monopoles dans le secteur des technologies géospatiales

### 5. Quelle est la différence entre les projections cartographiques équidistantes, conformes et équivalentes ?
- A. Les projections équidistantes préservent les distances, les projections conformes préservent les angles et les projections équivalentes préservent les aires.
- B. Les projections équidistantes préservent les angles, les projections conformes préservent les aires et les projections équivalentes préservent les distances.
- C. Les projections équidistantes préservent les aires, les projections conformes préservent les distances et les projections équivalentes préservent les angles.
- D. Les projections équidistantes préservent les formes, les projections conformes préservent les aires et les projections équivalentes préservent les distances.

### 6. Quelle fonction PostGIS peut être utilisée pour transformer une géométrie en un texte formaté selon la norme WKT?
- A. ST_AsText
- B. ST_AsBinary
- C. ST_AsGeoJSON
- D. ST_AsEWKT

### 7. Quelle est la meilleure requête SQL PostGIS pour trouver toutes les villes qui se trouvent à moins de 50 kilomètres d'une ville nommée 'Grenoble'?

- A.

```sql
SELECT nom_ville 
FROM villes 
WHERE ST_Distance(geom, (SELECT geom FROM villes WHERE nom_ville = 'Grenoble')) < 50000;
```

- B.

```sql
SELECT nom_ville 
FROM villes 
WHERE ST_Distance_Sphere(geom, (SELECT geom FROM villes WHERE nom_ville = 'Grenoble')) < 50;
```

- C.

```sql
SELECT nom_ville 
FROM villes 
WHERE ST_DWithin(geom, (SELECT geom FROM villes WHERE nom_ville = 'Grenoble'), 50000);
```

- D.

```sql
SELECT nom_ville 
FROM villes 
WHERE ST_Within(geom, (SELECT ST_Buffer(geom, 50 * 1000) FROM villes WHERE nom_ville = 'Grenoble'));
```

### 8. Quel est le type de géométrie qui n'est pas dans la spécification Simple Features de l'OGC?
- A. TIN
- B. Polygon
- C. Circle
- D. PolyhedralSurface

### 9. Quel est le type de données utilisé pour enregistrer les coordonnées SIG ?
- A. Integer
- B. Double
- C. Q32.32
- D. Float32

### 10. Pour un type double, quelle affirmation est juste ?
- A. 0.1 + 0.2 == 0.3
- B. (0.1 + 0.2) * 10 = 3.04
- C. 0.1 + 0.2 - 0.3 != 0.0
- D. 0.3 - 0.2 - 0.1 >= 0

### 11. Quelle requête SQL PostGIS peut être utilisée pour trouver la distance entre deux géométries?
- A. SELECT ST_Distance(geometry1, geometry2) FROM table_name;
- B. SELECT ST_Length(geometry1, geometry2) FROM table_name;
- C. SELECT ST_Area(geometry1, geometry2) FROM table_name;
- D. SELECT ST_Perimeter(geometry1, geometry2) FROM table_name;

### 12. Quel est le principe fondamental de la topologie dans les SIG ?
- A. Décrire des géométries à l'aide de formules mathématiques complexes.
- B. Définir des relations spatiales entre des entités géographiques.
- C. Utiliser des algorithmes de compression pour réduire la taille des données géospatiales.
- D. Créer des représentations visuelles des données géographiques.

### 13. Quel est le qualificatif de la représentation Simple Feature ?
- A. Topologique
- B. Spaghetti
- C. Relationel
- D. WKT

### 14. Quelle est la bonne représentation WKT d'une ligne ?
- A. Line ((0, 0), (1, 1))
- B. LineString ((0, 0), (1, 1))
- C. LineString (0 0, 1 1)
- D. LineString (0,0 1,1)

### 15. À quoi correspond la chaîne '01b90b0000000000000000f03f000000000000004000000000000008400000000000001040' ?
- A. PointZM(1 2 3 4)
- B. MultiPoint((1 2), (3 4))
- C. LineString((1 2), (3 4))
- D. PointZM(4 3 2 1)

### 16. Quelle fonction PostGIS peut être utilisée pour calculer l'intersection de plusieurs géométries?
- A. ST_Collect
- B. ST_Union
- C. ST_Intersects
- D. ST_Intersection

### 17. Pourquoi SELECT ST_Intersects(ST_Intersection(a.geom, b.geom), b.geom) peut retourner faux ?
- A. À cause des nombres à virgule
- B. Le résultat n'est pas déterminable
- C. La requête est fausse
- D. On ne peut pas utiliser deux fois la même géométrie dans une fonction

### 18. Dans quel ordre les coordonnées d'un polygone doivent-elles être enregistrées ?
- A. L'anneau extérieur et les anneaux intérieurs dans le même sens
- B. L'anneau extérieur dans un sens, les anneaux intérieurs dans un autre sens
- C. L'anneau extérieur dans un sens, les anneaux intérieurs alternativement dans un sens puis dans l'autre
- D. Aucune importance

### 19. Quel SIG est nativement topologique ?
- A. QGIS
- B. Mapinfo
- C. GRASS
- D. Philcarto

### 20. À quoi sert principalement la dimension M d'une géométrie ?
- A. À rien
- B. L'interpolation linéaire
- C. Ajouter une contre mesure à Z
- D. La réponse D

